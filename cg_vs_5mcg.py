from argparse import ArgumentParser
from pybedtools import BedTool
import pandas as pd
import seaborn as sns
from pyfaidx import Fasta
from itertools import chain, accumulate, islice, cycle

COLOR_PALETTE = sns.color_palette().as_hex()

def parse_arguments():
    parser = ArgumentParser(description='compare CG motif to 5mCG calls')
    parser.add_argument('cg')
    parser.add_argument('mcg')
    parser.add_argument('ref')
    parser.add_argument('output_prefix')
    return parser.parse_args()


def format_mcg(bed_file_path):
    with open(bed_file_path, 'r') as f:
        for line in f:
            i = line.rstrip().split('\t')
            if 'chr' in i[0]:
                yield i

def get_chromosome_sizes_from_ref(reference: str):
    """Extract chromosome sizes from reference FASTA

    Parameters
    ----------
    reference : str
        path to reference FASTA file

    Returns
    -------
    DataFrame
        name and size of each chromosome
    """

    return pd.DataFrame(((k, len(v)) for k, v in Fasta(reference).items()),
                        columns=('name', 'size'))


def generate_plotting_data(data_frame, sizes, scales, shifts):
    for _, r in data_frame.iterrows():
        chrom, pos, mcg, cg = r
        x = min(round(int(pos), -7), sizes[chrom])/sizes[chrom]*scales[chrom] + shifts[chrom]
        yield x, mcg, '5mCG', f'5mCG_{chrom}'
        yield x, cg, 'CGwin', f'CGwin_{chrom}'


def plot(data_frame, output, reference, chromosomes=[f'chr{c}' for c in tuple(range(1, 10))+('X',)],
         legend: bool = False, legend_title: str = 'Value',
         width: float = 8.0, color_palette=COLOR_PALETTE, alpha: float = 0.5,
         x_label: str = 'Chromosome',):
    """Generate a plot of average methylation levels across one or more
    chromosomes

    Parameters
    ----------
    output
        path to output file (pdf, png, or svg)
    reference
        path to FASTA file for reference genome
    chromosomes
        iterable of chromosomes to include in plot
    title : str
        title for plot
    legend : bool
        if true, draw a levend for the plot
    legend_title : str
        title for plot legend
    bin_size : int
        set bin size. The input <int> is converted to the bin size by the
        formula: 10^(<int>+6) bp. The default value is 0, i.e. 1-megabase bins.
    width : float
        width of plot in inches
    color_palette
        color palette for lines
    alpha
        transparency of lines
    """

    sizes = get_chromosome_sizes_from_ref(reference)
    sizes.index = sizes.name
    sizes = sizes.loc[chromosomes, 'size']
    scales = sizes / sizes.mean()
    shifts = pd.Series(accumulate(chain((0,),scales[:-1])), index=scales.index)
    plotting_data = pd.DataFrame(generate_plotting_data(data_frame, sizes, scales, shifts),
                                 columns=(x_label, 'Level (%)', legend_title,
                                          f'{legend_title}_chrom'))
    palette = tuple(islice(cycle(color_palette[:2]), len(chromosomes) * 2))
    ax = sns.lineplot(x=x_label, y='Level (%)',
                      hue=f'{legend_title}_chrom', data=plotting_data,
                      errorbar=None, linewidth=3, palette=palette,
                      alpha=alpha, legend=False)
    if (len(chromosomes) == 1) and (sizes is not None):
        xticks = ax.get_xticks()[1:-1]
        xlabels=tuple(f"{x*sizes.loc[chromosomes[0]]/1e6:.1f}" for x in xticks)
    else:
        xticks = shifts
        xlabels = chromosomes
    ax.set_xticks(xticks)
    ax.set_xticklabels(xlabels, rotation=30, ha='right')
    if legend:
        leg = ax.legend(bbox_to_anchor=(1.02, 1), loc='upper left',
                        borderaxespad=0, title=legend_title)
        for line in leg.get_lines():
            line.set_linewidth(3)
            line.set_alpha(alpha)
    fig = ax.get_figure()
    fig.set_figheight(3)
    fig.set_figwidth(width)
    fig.tight_layout()
    fig.savefig(output)
    fig.clf()



def main():
    args = parse_arguments()
    cg_bt = BedTool(args.cg)
    mcg_bt = BedTool(format_mcg(args.mcg)).saveas()
    intersect_bt = mcg_bt.intersect(cg_bt, wa=True, wb=True)
    grouped_bt = intersect_bt.groupby(g=(10, 11, 12), c=4, o=['mean'])
    intersect_bt = grouped_bt.intersect(cg_bt, wa=True, wb=True)
    plotting_data = pd.DataFrame(i.fields for i in intersect_bt).iloc[:,[0,1,3,8]]
    plotting_data.columns=('chromosome', 'position', 'CG methylation level (%)', 'CG window values (%)')
    plotting_data['chromosome'] = plotting_data['chromosome'].astype(str)
    plotting_data['position'] = plotting_data['position'].astype(int)
    plotting_data['CG methylation level (%)'] = plotting_data['CG methylation level (%)'].astype(float)
    plotting_data['CG window values (%)'] = plotting_data['CG window values (%)'].astype(float) / 10
    sns.set_context('talk')
    ax = sns.regplot(data=plotting_data, x='CG window values (%)', y='CG methylation level (%)')
    fig = ax.get_figure()
    fig.tight_layout()
    fig.savefig(f'{args.output_prefix}-reg.svg')
    fig.clf()
    ax = sns.kdeplot(data=plotting_data, x='CG window values (%)', y='CG methylation level (%)')
    fig = ax.get_figure()
    fig.tight_layout()
    fig.savefig(f'{args.output_prefix}-kde.svg')
    fig.clf()
    ax = sns.histplot(data=plotting_data, x='CG window values (%)', y='CG methylation level (%)')
    fig = ax.get_figure()
    fig.tight_layout()
    fig.savefig(f'{args.output_prefix}-hist.svg')
    fig.clf()
    plot(plotting_data, f'{args.output_prefix}-chrom.svg', reference=args.ref)


if __name__ == '__main__':
    main()
